fs = require('fs');
var parser = require('xml2json');

if (process.argv.length !== 4) {
    console.log("Uso.: node index.js { Nome do site } { Caminho para o XML } ");
    process.exit(1);
}

let file = process.argv[3];
let site = process.argv[2];

fs.readFile(file, function (err, data) {
    if (err) {
        console.log("Não foi possível abrir o arquivo " + file);
        process.exit(1);
    }
    try {
        let json = parser.toJson(data);
        json = JSON.parse(json);
        if (json.FileZilla3 === undefined) {
            console.log("O arquivo " + file + " não é um arquivo válido do FileZilla");
            process.exit(1);
        }
        let entries = json.FileZilla3.Servers;
        let text = false;
        for (server in entries) {
            if (server === "Server") {
                let servers = entries[server];
                for (entry in servers) {
                    if (servers[entry].Name === site && servers[entry].Pass) {
                        let data = servers[entry].Pass.$t;
                        text = Buffer.from(data, "base64").toString();
                    }
                    if (text) {
                        console.log(text);
                        process.exit(0);
                    }
                }
            }

            if (server === "Folder") {
                for (folder in entries[server]) {
                    for (s in entries[server][folder]) {
                        if (s === "Server") {
                            let serverInFolder = entries[server][folder][s];
                            for (entry in serverInFolder) {
                                if (serverInFolder[entry].Name === site && serverInFolder[entry].Pass) {
                                    let data = serverInFolder[entry].Pass.$t;
                                    text = Buffer.from(data, "base64").toString();
                                }
                                if (text) {
                                    console.log(text);
                                    process.exit(0);
                                }
                            }
                        }
                    }
                }
            }
        }

        if (!text) {
            console.log("O site " + site + " não existe no arquivo especificado");
            process.exit(1);
        }
    } catch (e) {
        console.log("Ocorreu um erro: " + e);
    }
});