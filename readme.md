## Dependências

- Node.JS

## Como utilizar

1. Clone o repositório para sua máquina.

2. Acesse a pasta através do terminal.

3. Execute o comando `npm install`.

4. Exporte o arquivo XML do FileZilla para algum local na máquina

5. No terminal, execute `node index.js { Nome do site } { Caminho para o XML }`, sendo o Nome do site idêntico à entrada do mesmo no FileZilla.